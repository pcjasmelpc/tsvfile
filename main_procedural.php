<?php
/**
 * User: jasmel
 * Date: 10/29/15
 * Time: 6:14 PM
 */



function tsvReadFile($fname)
{
    $infile = fopen($fname, 'r');
    if (!$infile)
        return false;

    $records = [];
    while (($record = fgetcsv($infile,0, "\t")) !== false) {
        $records[] = $record;
    }
    fclose($infile);
    return $records;
}

// array_multisort() is preffered over usort() for performance reasons.
function tsvSortColumn($rows, $sortColumn, $sortOrder = SORT_ASC)
{
    $sortCol = [];
    foreach ($rows as $k => $row) {

        $sortCol[] = $row[$sortColumn];
    }

    array_multisort($sortCol, $sortOrder,$rows);
    return $rows;
}

/*
 * Reads a TSV $file sorted as per $sortOrder of column at index $sortColumn.
 */
function tsvGetData($file, $sortColumn = 0, $sortOrder = SORT_ASC, $hasHeader = true)
{
    $rows = tsvReadFile($file);

    if ($rows) {
        $header = $hasHeader ? array_shift($rows) : range(1, count($rows[0]));
        $content = tsvSortColumn($rows, $sortColumn, $sortOrder);
        return [$header, $content];
    }
    return false;
}

$title = "Procedural";
$hasHeader = !isset($_GET['noheader']);
$DataFile = !empty(@$_GET['tsvfile']) ? $_GET['tsvfile'] : dirname("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") . '/country.csv';
$sortOrder = (@$_GET['desc'] == 'true' ? SORT_DESC : SORT_ASC);
$sortColumn = (@$_GET['sort'] ? intval($_GET['sort']) : 0);

list($header, $rows) = tsvGetData($DataFile, $sortColumn, $sortOrder, $hasHeader);

include('view.php');