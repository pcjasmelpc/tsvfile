<?php
/**
 * User: jasmel
 * Date: 10/29/15
 * Time: 7:02 PM
 */

class TSVFile{

    private $records, $header;

    public function __construct($fname, $hasHeader) {
        $this->open($fname, $hasHeader);
    }

    public function open($fname, $hasHeader){
        $infile = fopen($fname, 'r');
        if (!$infile)
            return false;

        $this->records = [];
        while (($record = fgetcsv($infile,0, "\t")) !== false) {
            $this->records[] = $record;
        }
        fclose($infile);

        if($hasHeader && count($this->records) > 0)
            $this->header = array_shift($this->records);
        else
            $this->header = range(1, count($this->records[0]));
    }

    public function sort($colIndex, $sortOrder=SORT_ASC){
        $sortCol = [];
        foreach ($this->records as $k => $row) {
            $sortCol[] = $row[$colIndex];
        }
        array_multisort($sortCol, $sortOrder, $this->records);
    }

    public function getData(){
        return $this->records;
    }

    public function getHeader(){
        return $this->header;
    }
}


$title = "OOP";
$DataFile = !empty(@$_GET['tsvfile']) ? $_GET['tsvfile'] : 'http://download.geonames.org/export/dump/timeZones.txt';
$sortOrder = (@$_GET['desc'] == 'true' ? SORT_DESC : SORT_ASC);
$sortColumn = (@$_GET['sort'] ? intval($_GET['sort']) : 0);
$hasHeader = !isset($_GET['noheader']);

$tvsFile = new TSVFile($DataFile, $hasHeader);
$tvsFile->sort($sortColumn, $sortOrder);
$header = $tvsFile->getHeader();
$rows = $tvsFile->getData();




include('view.php');