<!DOCTYPE html>
<html>
<head>
    <title>Sort Tab-delimited file : <?=$title?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style>
        table th.sort-asc:after { content: "\2191"; }
        table th.sort-desc:after { content: "\2193"; }
        table th:after { color: #123456; }
    </style>
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body class="container">
<nav class="row" style="margin:20px -20px;">
    <ul class="nav nav-tabs">
        <li <?=($title=='Procedural'?'class="active"':'')?>><a href="main_procedural.php">Procedural</a></li>
        <li <?=($title=='OOP'?'class="active"':'')?>><a href="main_oop.php">OOP</a></li>
    </ul>
</nav>
<div class="row" style="margin:20px -20px;">
    <form class="form-inline" method="get">
        <div class="form-group col-lg-8">
            <div class="input-group col-lg-12">
                <span class="input-group-addon" id="basic-addon1">TSV File</span>
                <input type="text" class="form-control" value="<?=$DataFile?>" name="tsvfile" >
            </div>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
        <label class="checkbox-inline"><input type="checkbox" name="noheader" <?=$hasHeader?'':'checked="checked"'?>> No Header</label>
    </form>
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <?php
        foreach($header as $i => $v)
            if($i==$sortColumn)
                echo '<th class="sort-'.($sortOrder==SORT_ASC?'asc':'desc').'">'.$v.'</th>';
            else
                echo "<th>$v</th>";
        ?>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($rows as $row) {
        echo '<tr>';
        foreach ($row as $i => $v)
            echo "<td>$v</td>";
        echo '</tr>';
    }
    ?>
    </tbody>
</table>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script>
    $('table th').click(function(){
        $('form').append(
            $("<input>").attr({type:"hidden", name:'sort'}).val($(this).index()),
            $("<input>").attr({type:"hidden", name:'desc'}).val($(this).hasClass('sort-asc'))
        ).submit();
    });
</script>
</body>
</html>
